---
layout: post
title:  "Hello GL World!"
date:   2016-02-13 22:00:04 -0200
categories: test
---
Let's see how it goes via Merge Request WIP

Hello @ramosmd

Hello world in _blue_
{: .blue}

<img src="https://virtuacreative.gitlab.io/images/forest.jpg" width="100%" height="auto">

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}