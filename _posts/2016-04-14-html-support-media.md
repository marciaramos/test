---
layout: post
title:  "HTML5 support for media files"
date:   2016-02-26 12:08:10 -0200
categories: test
---

# Audio Example

<audio controls>
  <source src="http://virtuacreative.gitlab.io/test/media/quirky-dog-kevin-macleod.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>

Attribution: [Music](https://www.youtube.com/watch?v=oUdZOgWD0DU) by [Kevin MacLeod](https://www.youtube.com/user/kmmusic) 


# Video Example

<figure id="iframe-video" class="video_container">
	<video controls="controls" allowfullscreen="true" poster="//blog.virtuacreative.com.br/assets/media/embed-youtube-poster.png">
  		<source src="//blog.virtuacreative.com.br/assets/media/embed-youtube.mp4" type="video/mp4">
	</video> 
</figure>

Attribution: Screencast by [Virtua Creative](https://virtuacreative.com.br) for [Virtua Creative Blog](//blog.virtuacreative.com.br/markdown-tips-tricks-part2.html)

Test: trigger build again
