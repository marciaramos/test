---
layout: post
title:  "GitLab CI - Stages"
date:   2016-02-17 22:00:04 -0200
categories: test
---

# GitLab CI - Stages

## Test 1

Testing two stages via `.gitlab-ci.yml` where `job: test` will run with `stage: test` and `job: pages` will run with `stage: deploy`.

When pushing to the branch `virtuacreative` the runner will do the job `test` and will build via `stage: test`.

When pushing to the branch `master`, the runner will do the `pages` job - will build via `stage: deploy` and will publish the post (whatever).


{% highlight yaml %}
image: ruby:2.1

before_script:
  - gem install bundler
  - gem install nokogiri -v '1.6.7.2' -- --use-system-libraries
  - bundle config build.nokogiri --use-system-libraries

test:
  script:
    - bundle install --path ~/bundle
    - bundle exec jekyll build -d public/
  artifacts:
    paths:
      - public
  stage: test
  only:
    - virtuacreative

pages:
  stage: deploy
  script:
    - bundle install --path ~/bundle
    - bundle exec jekyll build -d public/
  artifacts:
    paths:
      - public
  only:
    - master
{% endhighlight %}

----

## Test 2

Testing two stages via `.gitlab-ci.yml` where `job: test` will run with `stage: test` and `job: pages` will run with `stage: deploy`.

When pushing to **any branch**, the runner will do the job `test` and will build via `stage: test`.

When pushing to the branch `master`, the runner will do the `pages` job - will build via `stage: deploy` and will render the site.

{% highlight yaml %}

# ...

test:
# ...
  stage: test
  only:
    - branches@virtuacreative/test
  except:
    - master@virtuacreative/test

pages:
  stage: deploy
# ...
  only:
    - master
{% endhighlight %}

## Failed Test

Test failed when I tried to name the job as `deploy`- build passed but markdown file wasn't processed - wasn't published to the blog.

{% highlight yaml %}

# ...

deploy:
  stage: deploy
# ...
  only:
    - master
{% endhighlight %}


### Docs:

[GitLab CI - Stages](http://doc.gitlab.com/ce/ci/yaml/README.html#stages)